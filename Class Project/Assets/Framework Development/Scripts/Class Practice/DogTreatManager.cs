
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DogTreatManager : MonoBehaviour
{
    
    [SerializeField] private GameObject dogTreatPrefab;
    [SerializeField] int dogTreatCount = 5;

    [SerializeField] float dogTreatOffset = 0.5f;


    [SerializeField]List<GameObject> dogTreats = new List<GameObject>();


    private void Start()
    {
       SpawnDogTreats(); 
    }

    private void SpawnDogTreats()
    {


        for (var i = 0; i < dogTreatCount; i++)
        {
            GameObject newDogTreat = Instantiate(dogTreatPrefab);
            newDogTreat.transform.SetParent(transform);
            newDogTreat.transform.localPosition = Vector3.up * dogTreatOffset *i;
            newDogTreat.transform.localPosition += new Vector3(Random.Range(-0.3f, 0.3f),0,Random.Range(-0.3f, 0.3f));

            dogTreats.Add(newDogTreat);
        }
    }
}
