using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MySecondScript : MonoBehaviour
{
    // Start is called before the first frame update
    private int dogAge = 6;
    public bool isCute = true;
    void Start()
    {
        
    }

    public void LogName(string dogName)
    {
        Debug.Log("Dog name is " + dogName);
    }
    public int GetDogAge()
    {
        return dogAge;
    }
}
