using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IfStatements : MonoBehaviour
{
     [SerializeField] int age ;
        void Start()
    {
        if (age >= 6 && age <= 18)
        {
            Debug.Log("Go to school");
        }

        if (age < 0 || age > 150)
        {
            Debug.Log("You are probably not alive");
            if (age < -1000)
            {
                Debug.Log("You are a time traveller");
            }
        }

    }

    
}
