using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyFirstScript : MonoBehaviour
{
    private string dogName = "Susan";
    private MySecondScript mySecondScript;
    
    // Start is called before the first frame update
    void Start()
    {
        mySecondScript = GetComponent<MySecondScript>();
        LogMessage();
    }

    
    void LogMessage()
    {
        // Debug.Log("Name:  " + dogName);

        mySecondScript.LogName(dogName);
        Debug.Log("Age: " + mySecondScript.GetDogAge());
        Debug.Log("Is cute: " + mySecondScript.isCute);
    }

}
