using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICardPositioning : MonoBehaviour {
    [SerializeField] Transform activePlayerCards;
    [SerializeField] Transform activeDealerCards;
    [SerializeField] Transform playerCardPool;
    [SerializeField] Transform dealerCardPool;
    [SerializeField] float cardOffset;

    public void OnPlayerCardPosition (int childCount) {
        //take card from pool
        playerCardPool.GetChild (0).transform.parent = activePlayerCards;
        //set position at offset from previous card
        activePlayerCards.GetChild (childCount).transform.position = new Vector2 (activePlayerCards.GetChild (childCount - 1).transform.position.x + cardOffset, activePlayerCards.GetChild (0).transform.position.y);
    }
    public void OnDealerCardPosition (int childCount) {
        //take card from pool
        dealerCardPool.GetChild (0).transform.parent = activeDealerCards;
        //set position at offset from previous card
        activeDealerCards.GetChild (childCount).transform.position = new Vector2 (activeDealerCards.GetChild (childCount - 1).transform.position.x + cardOffset, activeDealerCards.GetChild (0).transform.position.y);
    }
    public void OnPositionsReset () {
        //put the active player cards back into the card pool
        for (var i = 0; i < activePlayerCards.childCount; i++) {
            activePlayerCards.GetChild (activePlayerCards.childCount - 1).parent = playerCardPool;
        }
        //put the active dealer cards back into the card pool
        for (var i = 0; i < activeDealerCards.childCount; i++) {
            activeDealerCards.GetChild (activeDealerCards.childCount - 1).parent = dealerCardPool;
        }

    }

}