using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UICardDisplay : MonoBehaviour
{
    [SerializeField] Transform playerActiveCards;
    [SerializeField] Transform dealerActiveCards;
   
    [SerializeField] CardMovementScript cardMovementScript;
    
    //this script receives the childcount of the child I need to modify, as well as the index of the sprite I need
    public void  OnPlayerCardDisplay(int cardIndex, int cardChild)
    {
        //get the card that has just been pulled and then set the sprite of it
        playerActiveCards.GetChild(cardChild).GetComponent<Image>().sprite = cardMovementScript.cardList[cardIndex];
        

    }
     public void  OnDealerCardDisplay(int cardIndex, int cardChild)
    {
        //get the card that has just been pulled and then set the sprite of it
        dealerActiveCards.GetChild(cardChild).GetComponent<Image>().sprite = cardMovementScript.cardList[cardIndex];
        

    }
}
