using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
public class TurnScript : MonoBehaviour {
    public bool dealerTurn;
    public bool playerTurn;
    [SerializeField] TextMeshProUGUI turnText;
    [SerializeField] ChipScript chipScript;
    [SerializeField] CardValueScript cardValueScript;
    [SerializeField] CardMovementScript cardMovementScript;

    private void Update () {

        //set text that says who's turn it is
        if (playerTurn) {
            turnText.text = "turn:player";
        } else if (dealerTurn) {
            turnText.text = "turn:dealer";
        }
    }

    // the player's turn is the turn where the game ends based on the win/lose condition
    public void OnPlayerTurn () {
        //set turn bools
        playerTurn = true;
        dealerTurn = false;
        //when its the last turn, decide what happens based on the win condition
        cardValueScript.OnEndGameActions ();

    }

    public void OnDealerTurn () {
        //set turn bools
        playerTurn = false;
        dealerTurn = true;
        //Determine the dealer's actions
        cardValueScript.OnDealerChoice ();
    }

}