using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ChipScript : MonoBehaviour {

    private float chipBalance;
    private float chipContainer;

    [SerializeField] float maxBet;
    [SerializeField] float minBet;
    [SerializeField] TextMeshProUGUI chipText;
    [SerializeField] TextMeshProUGUI chipContainerText;
    public float startBetCount;

    [SerializeField] CardMovementScript cardMovementScript;
    [SerializeField] TurnScript turnScript;
    [SerializeField] CardValueScript cardValueScript;
    private void Awake () {
        //this number represents the current turn number - it gets updated throughout the game and then reset
        startBetCount = 0;
        chipBalance = 1000;

    }

    private void Update () {
        OnChipContainerUpdate ();
        OnChipUpdate ();
    }

    // update chip balance in update
    private void OnChipUpdate () {

        chipText.text = "Chips: " + chipBalance;

    }
    private void OnChipContainerUpdate () {
        chipContainerText.text = "ChipContainer: " + chipContainer;
    }

    // start game bets
    public void OnMinBet () {

        if (startBetCount == 0) {
            //player turn set
            turnScript.OnPlayerTurn ();
            
            //bet gets added to container and also multiplied with dealer's bet
            chipContainer += minBet * 2;
            //start player shuffle
            cardMovementScript.OnCardShufflePlayer ();

        }

    }
    //increase betcount to keep track of what buttons can be pushed during the game
    public void OnIncreaseStartBetCountTo (int betCount) {
        startBetCount = betCount;
    }

    public void OnMaxBet () {

        if (startBetCount == 0) {
            //player turn set
            turnScript.OnPlayerTurn ();
           
            //bet gets added to container and also multiplied with dealer's bet
            chipContainer += maxBet * 2;
            //start player shuffle
            cardMovementScript.OnCardShufflePlayer ();

        }
    }

    public void OnWin () {
        //chipcontainer gets emptied and added to balance
        chipBalance += chipContainer;
        chipContainer = 0;
    }

    public void OnLose () {

        //chipcontainer gets emptied and subtracted from balance
        chipBalance -= chipContainer;
        chipContainer = 0;

    }

    public void OnTie () {
        //you get your chips back except the ones the dealer added(thats why its divided)
        chipBalance += chipContainer / 2;
        //chip container gets reset
        chipContainer = 0;
    }

    public void OnDouble () {
        //bet gets multiplied
        chipContainer *= 2;
    }

}