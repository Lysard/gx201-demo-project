using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CardValueScript : MonoBehaviour {
    public List<int> cardValueList = new List<int> ();
    private int cardValuePlayer1;
    private int cardValuePlayer2;

    private int cardValueDealer1;
    private int cardValueDealer2;

    public int cardValue;
    public int cardValueDealer;
    public bool playerBusts = false;
    public bool dealerBusts = false;
    public bool hasWon = false;
    public bool hasLost = false;
    public bool hasTied = false;

    [SerializeField] ChipScript chipScript;
    [SerializeField] TurnScript turnScript;
    [SerializeField] CardMovementScript cardMovementScript;
    [SerializeField] TextMeshProUGUI valueText;
    [SerializeField] TextMeshProUGUI valueTextDealer;
    [SerializeField] TextMeshProUGUI winLose;

    //generate card value list that corresponds to the card list in the movement script
    void Start () {
        OnCardValueList ();

    }
    // the first shuffle values set
    public void OnStartShufflePlayerValue (int cardIndex1, int cardIndex2) {

        //index of cardlist gets used to pull value from card value list
        cardValuePlayer1 = cardValueList[cardIndex1];
        //values get removed from list
        PlayerRemoveCards (cardIndex1);
        //second value
        cardValuePlayer2 = cardValueList[cardIndex2];
        PlayerRemoveCards (cardIndex2);
        //values get totaled
        cardValue = cardValuePlayer1 + cardValuePlayer2;
        //decrease if you get two aces
        OnAceCheck (cardValue);
        //set text
        valueText.text = cardValue.ToString ();

        //dealer shuffles now in the card movement script
        cardMovementScript.StartCoroutine (cardMovementScript.OnDealerShuffleWait ());
    }

    // starting cards dealt to dealer
    public void OnStartShuffleDealerValue (int card1, int card2) {
        //index of cardlist gets used to pull value from card value list
        cardValueDealer1 = cardValueList[card1];
        //values get removed from
        DealerRemoveCards (card1);
        cardValueDealer2 = cardValueList[card2];
        DealerRemoveCards (card2);
        //values get totaled
        cardValueDealer = cardValueDealer1 + cardValueDealer2;
        //check for 2 aces
        OnAceCheck (cardValueDealer);
        //set text
        valueTextDealer.text = cardValueDealer1.ToString ();

        //set player turn
        StartCoroutine (OnPlayerTurnWait ());
    }

    //check if you have two aces
    private void OnAceCheck (int cardValueCheck) {
        if (cardValueCheck > 21) {
            //two aces equal 12 now instead of 22
            cardValueCheck -= 10;
        }
    }

    //Set value of  player's choice

    public void OnHitPlayer (int index) {
        //get card value
        int tempHitValue = cardValueList[index];
        //update display value
        cardValue += tempHitValue;
        //update display value text
        valueText.text = cardValue.ToString ();
        //remove cards
        PlayerRemoveCards (index);

        // If over 21 you bust
        if (cardValue > 21) {
            //if I bust but get an ace - ace equals 1
            if (tempHitValue == 11) {
                cardValue -= 10;
                valueText.text = cardValue.ToString ();
            } else {
                //set bust bool
                playerBusts = true;

            }

        }
        //set dealer's turn
        cardMovementScript.StartCoroutine (cardMovementScript.OnDealerTurnWait ());
    }

    //Dealer choices

    //determine if the dealer should hit or not
    public void OnDealerChoice () {
        //if the player has already hit or stood
        if (chipScript.startBetCount >= 2) {
            //reveal dealer's card
            cardMovementScript.OnDealerReveal ();
            //dealer only makes their move if both the dealer and player don't bust
            if (playerBusts == false) {
                switch (cardValueDealer) {
                    //dealer hits if under 17
                    case <17:

                        //wait and then hit
                        StartCoroutine (OnWaitDealerHit ());

                        break;
                        // if the dealer doesn't have to hit and also hasn't busted then they give player the turn    
                    case >=17:
                        //check the win condition   
                        OnCheckWinCondition ();
                        break;

                }
            } else //if player busts, end game
            {
                OnCheckWinCondition ();

            }

        }
        //wait a sec before hitting       
        IEnumerator OnWaitDealerHit () {
            yield return new WaitForSeconds (3);

            //pull 3rd card
            cardMovementScript.OnDealerHit ();

        }
    }

    //dealer hits if they get under 17
    public void OnHitDealer (int index) {
        //if the dealer does not need to keep hitting
        
            //get card value   
            int tempHitValue = cardValueList[index];
            //update card display value
            cardValueDealer += tempHitValue;
            //update display
            valueTextDealer.text = cardValueDealer.ToString ();
            // remove the card from the list
            DealerRemoveCards (index);

            //check if the needs to rehit
            switch (cardValueDealer) {
                case <17:
                    //if the dealer gets under 17 when hitting - hit again
                    StartCoroutine (onDealerRehitTime ());
                    break;
                case >=17:
                    //if the dealer gets over 17 when hitting, check busting and give turn to player
                    if (cardValueDealer > 21) {
                        if (tempHitValue == 11) {
                            //check if the bust is an ace card, then decrease value
                            cardValueDealer -= 10;
                            valueTextDealer.text = cardValueDealer.ToString ();
                            OnCheckWinCondition ();

                        } else //if the bust is not an ace card dealer busts and win condition is set
                        {
                            dealerBusts = true;
                            OnCheckWinCondition ();
                        }
                    } else {
                        //set the win codition
                        OnCheckWinCondition ();
                    }
                    break;

            }
        

    }
    //if the dealer needs to hit again to get over 17
    public void OnHitAgain(int index)
    {
        cardValueDealer += cardValueList[index];
            valueTextDealer.text = cardValueDealer.ToString ();
            //the dealer doesn't need to hit again
            cardMovementScript.dealerUnder17 = false;
            //dealer bust condition
            if (cardValueDealer > 21) {
                dealerBusts = true;

            }
            //check win condition to end game if they didn't bus
            OnCheckWinCondition ();
    }

    //update dealer value with card reveal
    public void OnDealerRevealValue () {
        valueTextDealer.text = cardValueDealer.ToString ();
    }

    //set the win conditions
    public void OnCheckWinCondition () {
        //if its the last turn
        if (chipScript.startBetCount >= 2) {

            //what happens if the dealer doesn't bust
            if (!dealerBusts) {

                switch (playerBusts) {
                    case false: //player doesnt bust
                        if (cardValue < cardValueDealer) //player has less than dealer
                        {
                            hasLost = true;

                        }
                        if (cardValue > cardValueDealer) //player has more than dealer
                        {
                            hasWon = true;
                        }

                        if (cardValue == cardValueDealer) //values equal
                        {
                            hasTied = true;
                        }

                        break;
                    case true: //if player busts
                        hasLost = true;
                        hasWon = false;
                        break;
                }

                //turn goes to player where it will end the game and run the win/lose action 
                StartCoroutine (OnPlayerTurnWait ());
            }
            //what happens if the dealer busts
            else {
                //check if the player busts
                switch (playerBusts) {
                    case true: //if they busted, they lose too
                        hasWon = false;
                        hasLost = true;
                        break;
                    case false: //if they didn't bust, they win
                        hasWon = true;
                        hasLost = false;
                        break;

                }
                //turn goes to player where it will end the game and run the win/lose actions 
                StartCoroutine (OnPlayerTurnWait ());

            }

        }

    }

    //remove values with cards
    private void PlayerRemoveCards (int index) {

        cardValueList.RemoveAt (index);

    }
    private void DealerRemoveCards (int index) {
        cardValueList.RemoveAt (index);

    }

    //Wait before giving turn
    IEnumerator OnPlayerTurnWait () {
        yield return new WaitForSeconds (2);
        turnScript.OnPlayerTurn ();
    }
    //Wait before rehitting
    IEnumerator onDealerRehitTime () {
        yield return new WaitForSeconds (2);
        cardMovementScript.OnDealerUnder17 ();
    }

    //DETERMINE WHAT WIN/LOSE/TIE ACTIONS NEED TO BE CALLED

    public void OnEndGameActions () {
        //check win lose and then ends game
        if (chipScript.startBetCount == 2) {

            if (hasLost) {
                OnPlayerLose ();
            }
            if (hasWon) {
                OnPlayerWin ();
            }
            if (hasTied) {
                OnTie ();
            }

        }
    }

    //WIN/LOSE/TIE ACTIONS         

    // if tieing

    private void OnTie () {
        Debug.Log ("Player tie");
        //update chip container
        chipScript.OnTie ();
        //reset values
        OnGameEnd ();

    }

    // if winning

    private void OnPlayerWin () {
        // end the game when winning
        Debug.Log ("Player win");
        //set text
        winLose.text = "WON";
        //update chip container
        chipScript.OnWin ();
        //reset values
        OnGameEnd ();

    }

    // if losing

    private void OnPlayerLose () {
        // end the game when losing
        Debug.Log ("player lose");
        winLose.text = "Lost";
        //update chip container
        chipScript.OnLose ();
        //reset values
        OnGameEnd ();

    }
    public void OnGameEnd () {
        // the game resets all the values on the player's turn
        // it then fires the game end method in the cardmovement script
        if (turnScript.playerTurn) {
            cardMovementScript.OnGameEnd ();
            StartCoroutine (OnValueResets ());
        }
    }

    IEnumerator OnValueResets () {
        // wait a second before updating the value text
        yield return new WaitForSeconds (3);
        //reset values of cards
        cardValue = 0;
        cardValueDealer = 0;
        cardValuePlayer1 = 0;
        cardValuePlayer2 = 0;
        cardValueDealer1 = 0;
        cardValueDealer2 = 0;
        //update card value
        valueText.text = cardValue.ToString ();
        valueTextDealer.text = cardValueDealer.ToString ();
        //after the text is reset, destroy the list
        OnDestroyList ();
        //reset the turn counter
        chipScript.startBetCount = 0;
        //reset the win lose bools
        hasLost = false;
        hasWon = false;
        hasTied = false;
        dealerBusts = false;
        playerBusts = false;
        winLose.text = " ";
        //regenerate the card list
        OnCardValueList ();

    }

    //remove the values from the list
    private void OnDestroyList () {
        cardValueList.RemoveRange (0, cardValueList.Count);

    }

    //add the card values to the card value list in the order of the cardspritelist
    private void OnCardValueList () {

        for (var i = 0; i < 4; i++) {
            cardValueList.Add (11);
        }

        for (var i = 2; i < 11; i++) {
            for (var j = 0; j < 4; j++) {
                cardValueList.Add (i);

            }

        }

        for (var i = 0; i < 3; i++) {
            for (var j = 0; j < 4; j++) {
                cardValueList.Add (10);
            }
        }

    }
}