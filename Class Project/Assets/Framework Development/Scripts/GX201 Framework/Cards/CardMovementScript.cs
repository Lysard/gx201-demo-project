using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CardMovementScript : MonoBehaviour {
    public List<Sprite> cardList = new List<Sprite> ();
    [SerializeField] List<Sprite> cardSpriteList = new List<Sprite> ();
    private int cardIndexPlayer1;
    private int cardIndexPlayer2;
    private int cardIndexPlayer3;
    private int cardIndexDealer1;
    private int cardIndexDealer2;
    private int cardIndexDealer3;
    private int cardIndexDealer4;

    [SerializeField] Transform activePlayerCards;
    [SerializeField] Transform activeDealerCards;
    [SerializeField] Transform playerCardPool;
    [SerializeField] Transform dealerCardPool;

    [SerializeField] CardValueScript cardValueScript;
    [SerializeField] ChipScript chipScript;
    [SerializeField] TurnScript turnScript;
    [SerializeField] UICardPositioning uiPositioningScript;
    [SerializeField] UICardDisplay cardUIDisplayScript;
    [SerializeField] Transform dealerCoverTransform;
    public bool dealerUnder17 = false;

    //generate card list
    private void Awake () {
        OnCardList ();

    }

    //CARD SHUFFLE at start of game

    //player
    public void OnCardShufflePlayer () {
        if (turnScript.playerTurn) {

            // remove random cards from deck
            cardIndexPlayer1 = Random.Range (0, cardList.Count);

            // set sprites of the deactivated card in the active pool
            activePlayerCards.GetChild (0).gameObject.SetActive (true);
            cardUIDisplayScript.OnPlayerCardDisplay (cardIndexPlayer1, 0);
            //remove card
            PlayerRemoveCards (cardIndexPlayer1);
            //get second card
            cardIndexPlayer2 = Random.Range (0, cardList.Count);
            //set position
            uiPositioningScript.OnPlayerCardPosition (1);

            //set sprite
            cardUIDisplayScript.OnPlayerCardDisplay (cardIndexPlayer2, 1);
            //remove cards
            PlayerRemoveCards (cardIndexPlayer2);

            // insert index in value script to determine value
            cardValueScript.OnStartShufflePlayerValue (cardIndexPlayer1, cardIndexPlayer2);

        }

    }

    //Dealer shuffle
    public void OnCardShuffleDealer () {
        if (turnScript.dealerTurn) {
            //get random card
            cardIndexDealer1 = Random.Range (0, cardList.Count);
            activeDealerCards.GetChild (0).gameObject.SetActive (true);
            //Set card above dealer's second card, to hide it
            dealerCoverTransform.GetChild (0).gameObject.SetActive (true);
            //set sprite
            cardUIDisplayScript.OnDealerCardDisplay (cardIndexDealer1, 0);

            //remove dealer cards
            DealerRemoveCards (cardIndexDealer1);
            //get second card
            cardIndexDealer2 = Random.Range (0, cardList.Count);
            //set position
            uiPositioningScript.OnDealerCardPosition (1);

            cardUIDisplayScript.OnDealerCardDisplay (cardIndexDealer2, 1);
            //remove card
            DealerRemoveCards (cardIndexDealer2);
            //insert index in value script
            cardValueScript.OnStartShuffleDealerValue (cardIndexDealer1, cardIndexDealer2);

        }
    }

    //reveal dealer's card

    public void OnDealerReveal () {
        //disable the card that covers the second card
        dealerCoverTransform.GetChild (0).gameObject.SetActive (false);
        //update dealer value
        cardValueScript.OnDealerRevealValue ();
    }

    // code that runs when the lose/win conditions have been met
    public void OnGameEnd () {
        //if its the player's turn, and the last turn
        if (turnScript.playerTurn && chipScript.startBetCount == 2) {
            //all values get reset
            StartCoroutine (OnValueResets ());
        }

    }

    IEnumerator OnValueResets () {
        yield return new WaitForSeconds (3);
        uiPositioningScript.OnPositionsReset ();

        //Deactivate the remaining card in the active card pool
        activePlayerCards.GetChild (0).gameObject.SetActive (false);

        activeDealerCards.GetChild (0).gameObject.SetActive (false);

        

        //remove all cards from list
        cardList.RemoveRange (0, cardList.Count);
        OnCardList ();

    }

    // player choices Buttons

    public void OnStand () {
        if (turnScript.playerTurn && chipScript.startBetCount == 1) {
            //give turn to dealer
            StartCoroutine (OnDealerTurnWait ());
            chipScript.OnIncreaseStartBetCountTo (2);

        }
    }

    public void OnHit () {
        if (turnScript.playerTurn == true) {
            //only if first bet is played can I hit
            if (chipScript.startBetCount == 1) {

                //random card
                cardIndexPlayer3 = Random.Range (0, cardList.Count);
                //take card out of pool and place position
                uiPositioningScript.OnPlayerCardPosition (2);

                //set card sprite
                cardUIDisplayScript.OnPlayerCardDisplay (cardIndexPlayer3, 2);
                // remove card
                PlayerRemoveCards (cardIndexPlayer3);

                chipScript.OnIncreaseStartBetCountTo (2);

                //insert index in value script
                cardValueScript.OnHitPlayer (cardIndexPlayer3);

            }

        }

    }

    public void OnDouble () {
        chipScript.OnDouble ();
        OnHit ();
    }

    //DEALER ACTIONS

    //Dealer hits if they get under 17
    public void OnDealerHit () {
        // gets and sets the 3rd dealer card

        cardIndexDealer3 = Random.Range (0, cardList.Count);

        //set position
        uiPositioningScript.OnDealerCardPosition (2);
        cardUIDisplayScript.OnDealerCardDisplay (cardIndexDealer3, 2);

        DealerRemoveCards (cardIndexDealer3);

        //puts same index in the value script
        cardValueScript.OnHitDealer (cardIndexDealer3);

    }

    //if the dealer gets under 17 when they hit, take another card
    public void OnDealerUnder17 () {

        //get card
        cardIndexDealer4 = Random.Range (0, cardList.Count);

        //set position
        uiPositioningScript.OnDealerCardPosition (3);
        //sprite 
        cardUIDisplayScript.OnDealerCardDisplay (cardIndexDealer4, 3);
        //put index in script
        cardValueScript.OnHitAgain (cardIndexDealer4);
        //remove card
        DealerRemoveCards (cardIndexDealer4);

    }

    // removing the cards
    private void PlayerRemoveCards (int index) {
        cardList.RemoveAt (index);

    }
    private void DealerRemoveCards (int index) {
        cardList.RemoveAt (index);

    }
    // wait sec until shuffleing dealer cards too

    public IEnumerator OnDealerShuffleWait () {
        yield return new WaitForSeconds (2);
        turnScript.OnDealerTurn ();
        //hit now possible for player on next 
        chipScript.OnIncreaseStartBetCountTo (1);
        //shuffle dealer cards
        OnCardShuffleDealer ();
    }

    public IEnumerator OnDealerTurnWait () {
        yield return new WaitForSeconds (2);
        turnScript.OnDealerTurn ();

    }

    //card list adds the cards from sprite list
    public void OnCardList () {
        for (var i = 0; i < cardSpriteList.Count; i++) {
            cardList.Add (cardSpriteList[i]);

        }

    }
}